kops create cluster \
--name kops.speedyrogue.com \
--node-size t2.micro \
--node-count 2 \
--zones eu-west-2a \
--state s3://kops-nonprod-state-files/kops-state-files \
--target=terraform \
--yes

change node and control to associate_public_ip_address = false

terraform init
terraform plan 
terraform apply 


export KOPS_STATE_STORE=s3://kops-nonprod-state-files/kops-state-files # Get this values from config/<env_name>.tfvars
kops export kubecfg --admin --name kops.speedyrogue.com
kops validate cluster
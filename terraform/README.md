## IAC deployment 
####  Make sure you have your domain ready but we will do it in IAC way

##### Then run this command below.
###### This will create a kops cluster command output the terraform code inside a file kubernetes.tf where you can edit and add more configuration also a data folder where the launch template are located incase of autoscaling.
```
First is to create s3 bucket and let the statefile store localy 
then enable the backend to point to s3 
rerun terraform init so it can copy the state file to the created s3 (yes)
then continue to work on your project 

```

```
kops create cluster \
--name <name-of-your-cluster> \
--node-size t2.micro \
--node-count 2 \
--zones eu-west-2a \
--state <your s3 bucket url> \
--target=terraform \
--out=. \
--yes
```

change whats necessary in the terraform file
```
terraform init
terraform plan 
terraform apply (type yes)

```
Suggestions:
 * validate cluster: kops validate cluster --wait 10m
 * list nodes: kubectl get nodes --show-labels
 * ssh to a control-plane node: ssh -i ~/.ssh/id_rsa ubuntu@api.kops.speedyrogue.com
 * the ubuntu user is specific to Ubuntu. If not using Ubuntu please use the appropriate user based on your OS.
 * read about installing addons at: https://kops.sigs.k8s.io/addons.
```

kops edit cluster, edit and save it (:q!)
```
```
export KOPS_STATE_STORE=s3-bucket-url 
export NAME=kops-cluster-name
kops export kubecfg --admin=87600h --name <kops-cluster-name> --state <kops-state-file>
kops validate cluster 
or
kops validate cluster --state <kops-state-file>

kops export kubecfg --admin=87600h --name <kops-name>

kubectl get nodes.
```

Once this is completed move on to the micro-service repo to run the script build, test and push the image to repository code can be find here https://gitlab.com/joe_akinola/micro-service

follow the read me instruction there 

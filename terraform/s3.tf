resource "aws_s3_bucket" "kops-nonprod-state-files" {
  bucket = var.s3_kops_bucket_name

  tags = {
    Name        = "kops-nonprod-state-files"
  }
}

/* create s3 bucket objects (folders) */


resource "aws_s3_bucket_object" "kops-state-files" {
    bucket = aws_s3_bucket.kops-nonprod-state-files.id
    acl    = "private"
    key    = "kops-state-files/"

}
    
resource "aws_s3_bucket_object" "kops-terraform-state-file" {
    bucket = aws_s3_bucket.kops-nonprod-state-files.id
    acl    = "private"
    key    = "kops-terraform-state-files/"
}
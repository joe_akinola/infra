variable "kops_aws_region" {
  type = string
}

variable "s3_kops_bucket_name" {
  type = string
}


locals {
  cluster_name                 = "kops.speedyrogue.com"
  master_autoscaling_group_ids = [aws_autoscaling_group.control-plane-eu-west-2a-masters-kops-speedyrogue-com.id]
  master_security_group_ids    = [aws_security_group.masters-kops-speedyrogue-com.id]
  masters_role_arn             = aws_iam_role.masters-kops-speedyrogue-com.arn
  masters_role_name            = aws_iam_role.masters-kops-speedyrogue-com.name
  node_autoscaling_group_ids   = [aws_autoscaling_group.nodes-eu-west-2a-kops-speedyrogue-com.id]
  node_security_group_ids      = [aws_security_group.nodes-kops-speedyrogue-com.id]
  node_subnet_ids              = [aws_subnet.eu-west-2a-kops-speedyrogue-com.id]
  nodes_role_arn               = aws_iam_role.nodes-kops-speedyrogue-com.arn
  nodes_role_name              = aws_iam_role.nodes-kops-speedyrogue-com.name
  region                       = "eu-west-2"
  route_table_public_id        = aws_route_table.kops-speedyrogue-com.id
  subnet_eu-west-2a_id         = aws_subnet.eu-west-2a-kops-speedyrogue-com.id
  vpc_cidr_block               = aws_vpc.kops-speedyrogue-com.cidr_block
  vpc_id                       = aws_vpc.kops-speedyrogue-com.id
  vpc_ipv6_cidr_block          = aws_vpc.kops-speedyrogue-com.ipv6_cidr_block
  vpc_ipv6_cidr_length         = local.vpc_ipv6_cidr_block == "" ? null : tonumber(regex(".*/(\\d+)", local.vpc_ipv6_cidr_block)[0])
}

output "cluster_name" {
  value = "kops.speedyrogue.com"
}

output "master_autoscaling_group_ids" {
  value = [aws_autoscaling_group.control-plane-eu-west-2a-masters-kops-speedyrogue-com.id]
}

output "master_security_group_ids" {
  value = [aws_security_group.masters-kops-speedyrogue-com.id]
}

output "masters_role_arn" {
  value = aws_iam_role.masters-kops-speedyrogue-com.arn
}

output "masters_role_name" {
  value = aws_iam_role.masters-kops-speedyrogue-com.name
}

output "node_autoscaling_group_ids" {
  value = [aws_autoscaling_group.nodes-eu-west-2a-kops-speedyrogue-com.id]
}

output "node_security_group_ids" {
  value = [aws_security_group.nodes-kops-speedyrogue-com.id]
}

output "node_subnet_ids" {
  value = [aws_subnet.eu-west-2a-kops-speedyrogue-com.id]
}

output "nodes_role_arn" {
  value = aws_iam_role.nodes-kops-speedyrogue-com.arn
}

output "nodes_role_name" {
  value = aws_iam_role.nodes-kops-speedyrogue-com.name
}

output "region" {
  value = "eu-west-2"
}

output "route_table_public_id" {
  value = aws_route_table.kops-speedyrogue-com.id
}

output "subnet_eu-west-2a_id" {
  value = aws_subnet.eu-west-2a-kops-speedyrogue-com.id
}

output "vpc_cidr_block" {
  value = aws_vpc.kops-speedyrogue-com.cidr_block
}

output "vpc_id" {
  value = aws_vpc.kops-speedyrogue-com.id
}

output "vpc_ipv6_cidr_block" {
  value = aws_vpc.kops-speedyrogue-com.ipv6_cidr_block
}

output "vpc_ipv6_cidr_length" {
  value = local.vpc_ipv6_cidr_block == "" ? null : tonumber(regex(".*/(\\d+)", local.vpc_ipv6_cidr_block)[0])
}

provider "aws" {
  region = "eu-west-2"
}

provider "aws" {
  alias  = "files"
  region = "eu-west-2"
}

resource "aws_autoscaling_group" "control-plane-eu-west-2a-masters-kops-speedyrogue-com" {
  enabled_metrics = ["GroupDesiredCapacity", "GroupInServiceInstances", "GroupMaxSize", "GroupMinSize", "GroupPendingInstances", "GroupStandbyInstances", "GroupTerminatingInstances", "GroupTotalInstances"]
  launch_template {
    id      = aws_launch_template.control-plane-eu-west-2a-masters-kops-speedyrogue-com.id
    version = aws_launch_template.control-plane-eu-west-2a-masters-kops-speedyrogue-com.latest_version
  }
  max_instance_lifetime = 0
  max_size              = 1
  metrics_granularity   = "1Minute"
  min_size              = 1
  name                  = "control-plane-eu-west-2a.masters.kops.speedyrogue.com"
  protect_from_scale_in = false
  tag {
    key                 = "KubernetesCluster"
    propagate_at_launch = true
    value               = "kops.speedyrogue.com"
  }
  tag {
    key                 = "Name"
    propagate_at_launch = true
    value               = "control-plane-eu-west-2a.masters.kops.speedyrogue.com"
  }
  tag {
    key                 = "k8s.io/cluster-autoscaler/node-template/label/kops.k8s.io/kops-controller-pki"
    propagate_at_launch = true
    value               = ""
  }
  tag {
    key                 = "k8s.io/cluster-autoscaler/node-template/label/node-role.kubernetes.io/control-plane"
    propagate_at_launch = true
    value               = ""
  }
  tag {
    key                 = "k8s.io/cluster-autoscaler/node-template/label/node.kubernetes.io/exclude-from-external-load-balancers"
    propagate_at_launch = true
    value               = ""
  }
  tag {
    key                 = "k8s.io/role/control-plane"
    propagate_at_launch = true
    value               = "1"
  }
  tag {
    key                 = "k8s.io/role/master"
    propagate_at_launch = true
    value               = "1"
  }
  tag {
    key                 = "kops.k8s.io/instancegroup"
    propagate_at_launch = true
    value               = "control-plane-eu-west-2a"
  }
  tag {
    key                 = "kubernetes.io/cluster/kops.speedyrogue.com"
    propagate_at_launch = true
    value               = "owned"
  }
  vpc_zone_identifier = [aws_subnet.eu-west-2a-kops-speedyrogue-com.id]
}

resource "aws_autoscaling_group" "nodes-eu-west-2a-kops-speedyrogue-com" {
  enabled_metrics = ["GroupDesiredCapacity", "GroupInServiceInstances", "GroupMaxSize", "GroupMinSize", "GroupPendingInstances", "GroupStandbyInstances", "GroupTerminatingInstances", "GroupTotalInstances"]
  launch_template {
    id      = aws_launch_template.nodes-eu-west-2a-kops-speedyrogue-com.id
    version = aws_launch_template.nodes-eu-west-2a-kops-speedyrogue-com.latest_version
  }
  max_instance_lifetime = 0
  max_size              = 2
  metrics_granularity   = "1Minute"
  min_size              = 2
  name                  = "nodes-eu-west-2a.kops.speedyrogue.com"
  protect_from_scale_in = false
  tag {
    key                 = "KubernetesCluster"
    propagate_at_launch = true
    value               = "kops.speedyrogue.com"
  }
  tag {
    key                 = "Name"
    propagate_at_launch = true
    value               = "nodes-eu-west-2a.kops.speedyrogue.com"
  }
  tag {
    key                 = "k8s.io/cluster-autoscaler/node-template/label/node-role.kubernetes.io/node"
    propagate_at_launch = true
    value               = ""
  }
  tag {
    key                 = "k8s.io/role/node"
    propagate_at_launch = true
    value               = "1"
  }
  tag {
    key                 = "kops.k8s.io/instancegroup"
    propagate_at_launch = true
    value               = "nodes-eu-west-2a"
  }
  tag {
    key                 = "kubernetes.io/cluster/kops.speedyrogue.com"
    propagate_at_launch = true
    value               = "owned"
  }
  vpc_zone_identifier = [aws_subnet.eu-west-2a-kops-speedyrogue-com.id]
}

resource "aws_ebs_volume" "a-etcd-events-kops-speedyrogue-com" {
  availability_zone = "eu-west-2a"
  encrypted         = true
  iops              = 3000
  size              = 20
  tags = {
    "KubernetesCluster"                          = "kops.speedyrogue.com"
    "Name"                                       = "a.etcd-events.kops.speedyrogue.com"
    "k8s.io/etcd/events"                         = "a/a"
    "k8s.io/role/control-plane"                  = "1"
    "k8s.io/role/master"                         = "1"
    "kubernetes.io/cluster/kops.speedyrogue.com" = "owned"
  }
  throughput = 125
  type       = "gp3"
}

resource "aws_ebs_volume" "a-etcd-main-kops-speedyrogue-com" {
  availability_zone = "eu-west-2a"
  encrypted         = true
  iops              = 3000
  size              = 20
  tags = {
    "KubernetesCluster"                          = "kops.speedyrogue.com"
    "Name"                                       = "a.etcd-main.kops.speedyrogue.com"
    "k8s.io/etcd/main"                           = "a/a"
    "k8s.io/role/control-plane"                  = "1"
    "k8s.io/role/master"                         = "1"
    "kubernetes.io/cluster/kops.speedyrogue.com" = "owned"
  }
  throughput = 125
  type       = "gp3"
}

resource "aws_iam_instance_profile" "masters-kops-speedyrogue-com" {
  name = "masters.kops.speedyrogue.com"
  role = aws_iam_role.masters-kops-speedyrogue-com.name
  tags = {
    "KubernetesCluster"                          = "kops.speedyrogue.com"
    "Name"                                       = "masters.kops.speedyrogue.com"
    "kubernetes.io/cluster/kops.speedyrogue.com" = "owned"
  }
}

resource "aws_iam_instance_profile" "nodes-kops-speedyrogue-com" {
  name = "nodes.kops.speedyrogue.com"
  role = aws_iam_role.nodes-kops-speedyrogue-com.name
  tags = {
    "KubernetesCluster"                          = "kops.speedyrogue.com"
    "Name"                                       = "nodes.kops.speedyrogue.com"
    "kubernetes.io/cluster/kops.speedyrogue.com" = "owned"
  }
}

resource "aws_iam_role" "masters-kops-speedyrogue-com" {
  assume_role_policy = file("${path.module}/data/aws_iam_role_masters.kops.speedyrogue.com_policy")
  name               = "masters.kops.speedyrogue.com"
  tags = {
    "KubernetesCluster"                          = "kops.speedyrogue.com"
    "Name"                                       = "masters.kops.speedyrogue.com"
    "kubernetes.io/cluster/kops.speedyrogue.com" = "owned"
  }
}

resource "aws_iam_role" "nodes-kops-speedyrogue-com" {
  assume_role_policy = file("${path.module}/data/aws_iam_role_nodes.kops.speedyrogue.com_policy")
  name               = "nodes.kops.speedyrogue.com"
  tags = {
    "KubernetesCluster"                          = "kops.speedyrogue.com"
    "Name"                                       = "nodes.kops.speedyrogue.com"
    "kubernetes.io/cluster/kops.speedyrogue.com" = "owned"
  }
}

resource "aws_iam_role_policy" "masters-kops-speedyrogue-com" {
  name   = "masters.kops.speedyrogue.com"
  policy = file("${path.module}/data/aws_iam_role_policy_masters.kops.speedyrogue.com_policy")
  role   = aws_iam_role.masters-kops-speedyrogue-com.name
}

resource "aws_iam_role_policy" "nodes-kops-speedyrogue-com" {
  name   = "nodes.kops.speedyrogue.com"
  policy = file("${path.module}/data/aws_iam_role_policy_nodes.kops.speedyrogue.com_policy")
  role   = aws_iam_role.nodes-kops-speedyrogue-com.name
}

resource "aws_internet_gateway" "kops-speedyrogue-com" {
  tags = {
    "KubernetesCluster"                          = "kops.speedyrogue.com"
    "Name"                                       = "kops.speedyrogue.com"
    "kubernetes.io/cluster/kops.speedyrogue.com" = "owned"
  }
  vpc_id = aws_vpc.kops-speedyrogue-com.id
}

resource "aws_launch_template" "control-plane-eu-west-2a-masters-kops-speedyrogue-com" {
  block_device_mappings {
    device_name = "/dev/sda1"
    ebs {
      delete_on_termination = true
      encrypted             = true
      iops                  = 3000
      throughput            = 125
      volume_size           = 64
      volume_type           = "gp3"
    }
  }
  iam_instance_profile {
    name = aws_iam_instance_profile.masters-kops-speedyrogue-com.id
  }
  image_id      = "ami-059ddb696446729d4"
  instance_type = "t3.medium"
  lifecycle {
    create_before_destroy = true
  }
  metadata_options {
    http_endpoint               = "enabled"
    http_protocol_ipv6          = "disabled"
    http_put_response_hop_limit = 1
    http_tokens                 = "required"
  }
  monitoring {
    enabled = false
  }
  name = "control-plane-eu-west-2a.masters.kops.speedyrogue.com"
  network_interfaces {
    associate_public_ip_address = true
    delete_on_termination       = true
    ipv6_address_count          = 0
    security_groups             = [aws_security_group.masters-kops-speedyrogue-com.id]
  }
  tag_specifications {
    resource_type = "instance"
    tags = {
      "KubernetesCluster"                                                                                     = "kops.speedyrogue.com"
      "Name"                                                                                                  = "control-plane-eu-west-2a.masters.kops.speedyrogue.com"
      "k8s.io/cluster-autoscaler/node-template/label/kops.k8s.io/kops-controller-pki"                         = ""
      "k8s.io/cluster-autoscaler/node-template/label/node-role.kubernetes.io/control-plane"                   = ""
      "k8s.io/cluster-autoscaler/node-template/label/node.kubernetes.io/exclude-from-external-load-balancers" = ""
      "k8s.io/role/control-plane"                                                                             = "1"
      "k8s.io/role/master"                                                                                    = "1"
      "kops.k8s.io/instancegroup"                                                                             = "control-plane-eu-west-2a"
      "kubernetes.io/cluster/kops.speedyrogue.com"                                                            = "owned"
    }
  }
  tag_specifications {
    resource_type = "volume"
    tags = {
      "KubernetesCluster"                                                                                     = "kops.speedyrogue.com"
      "Name"                                                                                                  = "control-plane-eu-west-2a.masters.kops.speedyrogue.com"
      "k8s.io/cluster-autoscaler/node-template/label/kops.k8s.io/kops-controller-pki"                         = ""
      "k8s.io/cluster-autoscaler/node-template/label/node-role.kubernetes.io/control-plane"                   = ""
      "k8s.io/cluster-autoscaler/node-template/label/node.kubernetes.io/exclude-from-external-load-balancers" = ""
      "k8s.io/role/control-plane"                                                                             = "1"
      "k8s.io/role/master"                                                                                    = "1"
      "kops.k8s.io/instancegroup"                                                                             = "control-plane-eu-west-2a"
      "kubernetes.io/cluster/kops.speedyrogue.com"                                                            = "owned"
    }
  }
  tags = {
    "KubernetesCluster"                                                                                     = "kops.speedyrogue.com"
    "Name"                                                                                                  = "control-plane-eu-west-2a.masters.kops.speedyrogue.com"
    "k8s.io/cluster-autoscaler/node-template/label/kops.k8s.io/kops-controller-pki"                         = ""
    "k8s.io/cluster-autoscaler/node-template/label/node-role.kubernetes.io/control-plane"                   = ""
    "k8s.io/cluster-autoscaler/node-template/label/node.kubernetes.io/exclude-from-external-load-balancers" = ""
    "k8s.io/role/control-plane"                                                                             = "1"
    "k8s.io/role/master"                                                                                    = "1"
    "kops.k8s.io/instancegroup"                                                                             = "control-plane-eu-west-2a"
    "kubernetes.io/cluster/kops.speedyrogue.com"                                                            = "owned"
  }
  user_data = filebase64("${path.module}/data/aws_launch_template_control-plane-eu-west-2a.masters.kops.speedyrogue.com_user_data")
}

resource "aws_launch_template" "nodes-eu-west-2a-kops-speedyrogue-com" {
  block_device_mappings {
    device_name = "/dev/sda1"
    ebs {
      delete_on_termination = true
      encrypted             = true
      iops                  = 3000
      throughput            = 125
      volume_size           = 128
      volume_type           = "gp3"
    }
  }
  iam_instance_profile {
    name = aws_iam_instance_profile.nodes-kops-speedyrogue-com.id
  }
  image_id      = "ami-059ddb696446729d4"
  instance_type = "t2.micro"
  lifecycle {
    create_before_destroy = true
  }
  metadata_options {
    http_endpoint               = "enabled"
    http_protocol_ipv6          = "disabled"
    http_put_response_hop_limit = 1
    http_tokens                 = "required"
  }
  monitoring {
    enabled = false
  }
  name = "nodes-eu-west-2a.kops.speedyrogue.com"
  network_interfaces {
    associate_public_ip_address = true
    delete_on_termination       = true
    ipv6_address_count          = 0
    security_groups             = [aws_security_group.nodes-kops-speedyrogue-com.id]
  }
  tag_specifications {
    resource_type = "instance"
    tags = {
      "KubernetesCluster"                                                          = "kops.speedyrogue.com"
      "Name"                                                                       = "nodes-eu-west-2a.kops.speedyrogue.com"
      "k8s.io/cluster-autoscaler/node-template/label/node-role.kubernetes.io/node" = ""
      "k8s.io/role/node"                                                           = "1"
      "kops.k8s.io/instancegroup"                                                  = "nodes-eu-west-2a"
      "kubernetes.io/cluster/kops.speedyrogue.com"                                 = "owned"
    }
  }
  tag_specifications {
    resource_type = "volume"
    tags = {
      "KubernetesCluster"                                                          = "kops.speedyrogue.com"
      "Name"                                                                       = "nodes-eu-west-2a.kops.speedyrogue.com"
      "k8s.io/cluster-autoscaler/node-template/label/node-role.kubernetes.io/node" = ""
      "k8s.io/role/node"                                                           = "1"
      "kops.k8s.io/instancegroup"                                                  = "nodes-eu-west-2a"
      "kubernetes.io/cluster/kops.speedyrogue.com"                                 = "owned"
    }
  }
  tags = {
    "KubernetesCluster"                                                          = "kops.speedyrogue.com"
    "Name"                                                                       = "nodes-eu-west-2a.kops.speedyrogue.com"
    "k8s.io/cluster-autoscaler/node-template/label/node-role.kubernetes.io/node" = ""
    "k8s.io/role/node"                                                           = "1"
    "kops.k8s.io/instancegroup"                                                  = "nodes-eu-west-2a"
    "kubernetes.io/cluster/kops.speedyrogue.com"                                 = "owned"
  }
  user_data = filebase64("${path.module}/data/aws_launch_template_nodes-eu-west-2a.kops.speedyrogue.com_user_data")
}

resource "aws_route" "route-0-0-0-0--0" {
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.kops-speedyrogue-com.id
  route_table_id         = aws_route_table.kops-speedyrogue-com.id
}

resource "aws_route" "route-__--0" {
  destination_ipv6_cidr_block = "::/0"
  gateway_id                  = aws_internet_gateway.kops-speedyrogue-com.id
  route_table_id              = aws_route_table.kops-speedyrogue-com.id
}

resource "aws_route_table" "kops-speedyrogue-com" {
  tags = {
    "KubernetesCluster"                          = "kops.speedyrogue.com"
    "Name"                                       = "kops.speedyrogue.com"
    "kubernetes.io/cluster/kops.speedyrogue.com" = "owned"
    "kubernetes.io/kops/role"                    = "public"
  }
  vpc_id = aws_vpc.kops-speedyrogue-com.id
}

resource "aws_route_table_association" "eu-west-2a-kops-speedyrogue-com" {
  route_table_id = aws_route_table.kops-speedyrogue-com.id
  subnet_id      = aws_subnet.eu-west-2a-kops-speedyrogue-com.id
}

resource "aws_s3_object" "cluster-completed-spec" {
  bucket   = "kops-nonprod-state-files"
  content  = file("${path.module}/data/aws_s3_object_cluster-completed.spec_content")
  key      = "kops-state-files/kops.speedyrogue.com/cluster-completed.spec"
  provider = aws.files
}

resource "aws_s3_object" "etcd-cluster-spec-events" {
  bucket   = "kops-nonprod-state-files"
  content  = file("${path.module}/data/aws_s3_object_etcd-cluster-spec-events_content")
  key      = "kops-state-files/kops.speedyrogue.com/backups/etcd/events/control/etcd-cluster-spec"
  provider = aws.files
}

resource "aws_s3_object" "etcd-cluster-spec-main" {
  bucket   = "kops-nonprod-state-files"
  content  = file("${path.module}/data/aws_s3_object_etcd-cluster-spec-main_content")
  key      = "kops-state-files/kops.speedyrogue.com/backups/etcd/main/control/etcd-cluster-spec"
  provider = aws.files
}

resource "aws_s3_object" "kops-speedyrogue-com-addons-aws-cloud-controller-addons-k8s-io-k8s-1-18" {
  bucket   = "kops-nonprod-state-files"
  content  = file("${path.module}/data/aws_s3_object_kops.speedyrogue.com-addons-aws-cloud-controller.addons.k8s.io-k8s-1.18_content")
  key      = "kops-state-files/kops.speedyrogue.com/addons/aws-cloud-controller.addons.k8s.io/k8s-1.18.yaml"
  provider = aws.files
}

resource "aws_s3_object" "kops-speedyrogue-com-addons-aws-ebs-csi-driver-addons-k8s-io-k8s-1-17" {
  bucket   = "kops-nonprod-state-files"
  content  = file("${path.module}/data/aws_s3_object_kops.speedyrogue.com-addons-aws-ebs-csi-driver.addons.k8s.io-k8s-1.17_content")
  key      = "kops-state-files/kops.speedyrogue.com/addons/aws-ebs-csi-driver.addons.k8s.io/k8s-1.17.yaml"
  provider = aws.files
}

resource "aws_s3_object" "kops-speedyrogue-com-addons-bootstrap" {
  bucket   = "kops-nonprod-state-files"
  content  = file("${path.module}/data/aws_s3_object_kops.speedyrogue.com-addons-bootstrap_content")
  key      = "kops-state-files/kops.speedyrogue.com/addons/bootstrap-channel.yaml"
  provider = aws.files
}

resource "aws_s3_object" "kops-speedyrogue-com-addons-coredns-addons-k8s-io-k8s-1-12" {
  bucket   = "kops-nonprod-state-files"
  content  = file("${path.module}/data/aws_s3_object_kops.speedyrogue.com-addons-coredns.addons.k8s.io-k8s-1.12_content")
  key      = "kops-state-files/kops.speedyrogue.com/addons/coredns.addons.k8s.io/k8s-1.12.yaml"
  provider = aws.files
}

resource "aws_s3_object" "kops-speedyrogue-com-addons-dns-controller-addons-k8s-io-k8s-1-12" {
  bucket   = "kops-nonprod-state-files"
  content  = file("${path.module}/data/aws_s3_object_kops.speedyrogue.com-addons-dns-controller.addons.k8s.io-k8s-1.12_content")
  key      = "kops-state-files/kops.speedyrogue.com/addons/dns-controller.addons.k8s.io/k8s-1.12.yaml"
  provider = aws.files
}

resource "aws_s3_object" "kops-speedyrogue-com-addons-kops-controller-addons-k8s-io-k8s-1-16" {
  bucket   = "kops-nonprod-state-files"
  content  = file("${path.module}/data/aws_s3_object_kops.speedyrogue.com-addons-kops-controller.addons.k8s.io-k8s-1.16_content")
  key      = "kops-state-files/kops.speedyrogue.com/addons/kops-controller.addons.k8s.io/k8s-1.16.yaml"
  provider = aws.files
}

resource "aws_s3_object" "kops-speedyrogue-com-addons-kubelet-api-rbac-addons-k8s-io-k8s-1-9" {
  bucket   = "kops-nonprod-state-files"
  content  = file("${path.module}/data/aws_s3_object_kops.speedyrogue.com-addons-kubelet-api.rbac.addons.k8s.io-k8s-1.9_content")
  key      = "kops-state-files/kops.speedyrogue.com/addons/kubelet-api.rbac.addons.k8s.io/k8s-1.9.yaml"
  provider = aws.files
}

resource "aws_s3_object" "kops-speedyrogue-com-addons-limit-range-addons-k8s-io" {
  bucket   = "kops-nonprod-state-files"
  content  = file("${path.module}/data/aws_s3_object_kops.speedyrogue.com-addons-limit-range.addons.k8s.io_content")
  key      = "kops-state-files/kops.speedyrogue.com/addons/limit-range.addons.k8s.io/v1.5.0.yaml"
  provider = aws.files
}

resource "aws_s3_object" "kops-speedyrogue-com-addons-networking-cilium-io-k8s-1-16" {
  bucket   = "kops-nonprod-state-files"
  content  = file("${path.module}/data/aws_s3_object_kops.speedyrogue.com-addons-networking.cilium.io-k8s-1.16_content")
  key      = "kops-state-files/kops.speedyrogue.com/addons/networking.cilium.io/k8s-1.16-v1.12.yaml"
  provider = aws.files
}

resource "aws_s3_object" "kops-speedyrogue-com-addons-storage-aws-addons-k8s-io-v1-15-0" {
  bucket   = "kops-nonprod-state-files"
  content  = file("${path.module}/data/aws_s3_object_kops.speedyrogue.com-addons-storage-aws.addons.k8s.io-v1.15.0_content")
  key      = "kops-state-files/kops.speedyrogue.com/addons/storage-aws.addons.k8s.io/v1.15.0.yaml"
  provider = aws.files
}

resource "aws_s3_object" "kops-version-txt" {
  bucket   = "kops-nonprod-state-files"
  content  = file("${path.module}/data/aws_s3_object_kops-version.txt_content")
  key      = "kops-state-files/kops.speedyrogue.com/kops-version.txt"
  provider = aws.files
}

resource "aws_s3_object" "manifests-etcdmanager-events-control-plane-eu-west-2a" {
  bucket   = "kops-nonprod-state-files"
  content  = file("${path.module}/data/aws_s3_object_manifests-etcdmanager-events-control-plane-eu-west-2a_content")
  key      = "kops-state-files/kops.speedyrogue.com/manifests/etcd/events-control-plane-eu-west-2a.yaml"
  provider = aws.files
}

resource "aws_s3_object" "manifests-etcdmanager-main-control-plane-eu-west-2a" {
  bucket   = "kops-nonprod-state-files"
  content  = file("${path.module}/data/aws_s3_object_manifests-etcdmanager-main-control-plane-eu-west-2a_content")
  key      = "kops-state-files/kops.speedyrogue.com/manifests/etcd/main-control-plane-eu-west-2a.yaml"
  provider = aws.files
}

resource "aws_s3_object" "manifests-static-kube-apiserver-healthcheck" {
  bucket   = "kops-nonprod-state-files"
  content  = file("${path.module}/data/aws_s3_object_manifests-static-kube-apiserver-healthcheck_content")
  key      = "kops-state-files/kops.speedyrogue.com/manifests/static/kube-apiserver-healthcheck.yaml"
  provider = aws.files
}

resource "aws_s3_object" "nodeupconfig-control-plane-eu-west-2a" {
  bucket   = "kops-nonprod-state-files"
  content  = file("${path.module}/data/aws_s3_object_nodeupconfig-control-plane-eu-west-2a_content")
  key      = "kops-state-files/kops.speedyrogue.com/igconfig/control-plane/control-plane-eu-west-2a/nodeupconfig.yaml"
  provider = aws.files
}

resource "aws_s3_object" "nodeupconfig-nodes-eu-west-2a" {
  bucket   = "kops-nonprod-state-files"
  content  = file("${path.module}/data/aws_s3_object_nodeupconfig-nodes-eu-west-2a_content")
  key      = "kops-state-files/kops.speedyrogue.com/igconfig/node/nodes-eu-west-2a/nodeupconfig.yaml"
  provider = aws.files
}

resource "aws_security_group" "masters-kops-speedyrogue-com" {
  description = "Security group for masters"
  name        = "masters.kops.speedyrogue.com"
  tags = {
    "KubernetesCluster"                          = "kops.speedyrogue.com"
    "Name"                                       = "masters.kops.speedyrogue.com"
    "kubernetes.io/cluster/kops.speedyrogue.com" = "owned"
  }
  vpc_id = aws_vpc.kops-speedyrogue-com.id
}

resource "aws_security_group" "nodes-kops-speedyrogue-com" {
  description = "Security group for nodes"
  name        = "nodes.kops.speedyrogue.com"
  tags = {
    "KubernetesCluster"                          = "kops.speedyrogue.com"
    "Name"                                       = "nodes.kops.speedyrogue.com"
    "kubernetes.io/cluster/kops.speedyrogue.com" = "owned"
  }
  vpc_id = aws_vpc.kops-speedyrogue-com.id
}

resource "aws_security_group_rule" "from-0-0-0-0--0-ingress-tcp-22to22-masters-kops-speedyrogue-com" {
  cidr_blocks       = ["0.0.0.0/0"]
  from_port         = 22
  protocol          = "tcp"
  security_group_id = aws_security_group.masters-kops-speedyrogue-com.id
  to_port           = 22
  type              = "ingress"
}

resource "aws_security_group_rule" "from-0-0-0-0--0-ingress-tcp-22to22-nodes-kops-speedyrogue-com" {
  cidr_blocks       = ["0.0.0.0/0"]
  from_port         = 22
  protocol          = "tcp"
  security_group_id = aws_security_group.nodes-kops-speedyrogue-com.id
  to_port           = 22
  type              = "ingress"
}

resource "aws_security_group_rule" "from-0-0-0-0--0-ingress-tcp-443to443-masters-kops-speedyrogue-com" {
  cidr_blocks       = ["0.0.0.0/0"]
  from_port         = 443
  protocol          = "tcp"
  security_group_id = aws_security_group.masters-kops-speedyrogue-com.id
  to_port           = 443
  type              = "ingress"
}

resource "aws_security_group_rule" "from-__--0-ingress-tcp-22to22-masters-kops-speedyrogue-com" {
  from_port         = 22
  ipv6_cidr_blocks  = ["::/0"]
  protocol          = "tcp"
  security_group_id = aws_security_group.masters-kops-speedyrogue-com.id
  to_port           = 22
  type              = "ingress"
}

resource "aws_security_group_rule" "from-__--0-ingress-tcp-22to22-nodes-kops-speedyrogue-com" {
  from_port         = 22
  ipv6_cidr_blocks  = ["::/0"]
  protocol          = "tcp"
  security_group_id = aws_security_group.nodes-kops-speedyrogue-com.id
  to_port           = 22
  type              = "ingress"
}

resource "aws_security_group_rule" "from-__--0-ingress-tcp-443to443-masters-kops-speedyrogue-com" {
  from_port         = 443
  ipv6_cidr_blocks  = ["::/0"]
  protocol          = "tcp"
  security_group_id = aws_security_group.masters-kops-speedyrogue-com.id
  to_port           = 443
  type              = "ingress"
}

resource "aws_security_group_rule" "from-masters-kops-speedyrogue-com-egress-all-0to0-0-0-0-0--0" {
  cidr_blocks       = ["0.0.0.0/0"]
  from_port         = 0
  protocol          = "-1"
  security_group_id = aws_security_group.masters-kops-speedyrogue-com.id
  to_port           = 0
  type              = "egress"
}

resource "aws_security_group_rule" "from-masters-kops-speedyrogue-com-egress-all-0to0-__--0" {
  from_port         = 0
  ipv6_cidr_blocks  = ["::/0"]
  protocol          = "-1"
  security_group_id = aws_security_group.masters-kops-speedyrogue-com.id
  to_port           = 0
  type              = "egress"
}

resource "aws_security_group_rule" "from-masters-kops-speedyrogue-com-ingress-all-0to0-masters-kops-speedyrogue-com" {
  from_port                = 0
  protocol                 = "-1"
  security_group_id        = aws_security_group.masters-kops-speedyrogue-com.id
  source_security_group_id = aws_security_group.masters-kops-speedyrogue-com.id
  to_port                  = 0
  type                     = "ingress"
}

resource "aws_security_group_rule" "from-masters-kops-speedyrogue-com-ingress-all-0to0-nodes-kops-speedyrogue-com" {
  from_port                = 0
  protocol                 = "-1"
  security_group_id        = aws_security_group.nodes-kops-speedyrogue-com.id
  source_security_group_id = aws_security_group.masters-kops-speedyrogue-com.id
  to_port                  = 0
  type                     = "ingress"
}

resource "aws_security_group_rule" "from-nodes-kops-speedyrogue-com-egress-all-0to0-0-0-0-0--0" {
  cidr_blocks       = ["0.0.0.0/0"]
  from_port         = 0
  protocol          = "-1"
  security_group_id = aws_security_group.nodes-kops-speedyrogue-com.id
  to_port           = 0
  type              = "egress"
}

resource "aws_security_group_rule" "from-nodes-kops-speedyrogue-com-egress-all-0to0-__--0" {
  from_port         = 0
  ipv6_cidr_blocks  = ["::/0"]
  protocol          = "-1"
  security_group_id = aws_security_group.nodes-kops-speedyrogue-com.id
  to_port           = 0
  type              = "egress"
}

resource "aws_security_group_rule" "from-nodes-kops-speedyrogue-com-ingress-all-0to0-nodes-kops-speedyrogue-com" {
  from_port                = 0
  protocol                 = "-1"
  security_group_id        = aws_security_group.nodes-kops-speedyrogue-com.id
  source_security_group_id = aws_security_group.nodes-kops-speedyrogue-com.id
  to_port                  = 0
  type                     = "ingress"
}

resource "aws_security_group_rule" "from-nodes-kops-speedyrogue-com-ingress-tcp-1to2379-masters-kops-speedyrogue-com" {
  from_port                = 1
  protocol                 = "tcp"
  security_group_id        = aws_security_group.masters-kops-speedyrogue-com.id
  source_security_group_id = aws_security_group.nodes-kops-speedyrogue-com.id
  to_port                  = 2379
  type                     = "ingress"
}

resource "aws_security_group_rule" "from-nodes-kops-speedyrogue-com-ingress-tcp-2382to4000-masters-kops-speedyrogue-com" {
  from_port                = 2382
  protocol                 = "tcp"
  security_group_id        = aws_security_group.masters-kops-speedyrogue-com.id
  source_security_group_id = aws_security_group.nodes-kops-speedyrogue-com.id
  to_port                  = 4000
  type                     = "ingress"
}

resource "aws_security_group_rule" "from-nodes-kops-speedyrogue-com-ingress-tcp-4003to65535-masters-kops-speedyrogue-com" {
  from_port                = 4003
  protocol                 = "tcp"
  security_group_id        = aws_security_group.masters-kops-speedyrogue-com.id
  source_security_group_id = aws_security_group.nodes-kops-speedyrogue-com.id
  to_port                  = 65535
  type                     = "ingress"
}

resource "aws_security_group_rule" "from-nodes-kops-speedyrogue-com-ingress-udp-1to65535-masters-kops-speedyrogue-com" {
  from_port                = 1
  protocol                 = "udp"
  security_group_id        = aws_security_group.masters-kops-speedyrogue-com.id
  source_security_group_id = aws_security_group.nodes-kops-speedyrogue-com.id
  to_port                  = 65535
  type                     = "ingress"
}

resource "aws_subnet" "eu-west-2a-kops-speedyrogue-com" {
  availability_zone                           = "eu-west-2a"
  cidr_block                                  = "172.20.32.0/19"
  enable_resource_name_dns_a_record_on_launch = true
  private_dns_hostname_type_on_launch         = "resource-name"
  tags = {
    "KubernetesCluster"                                   = "kops.speedyrogue.com"
    "Name"                                                = "eu-west-2a.kops.speedyrogue.com"
    "SubnetType"                                          = "Public"
    "kops.k8s.io/instance-group/control-plane-eu-west-2a" = "true"
    "kops.k8s.io/instance-group/nodes-eu-west-2a"         = "true"
    "kubernetes.io/cluster/kops.speedyrogue.com"          = "owned"
    "kubernetes.io/role/elb"                              = "1"
    "kubernetes.io/role/internal-elb"                     = "1"
  }
  vpc_id = aws_vpc.kops-speedyrogue-com.id
}

resource "aws_vpc" "kops-speedyrogue-com" {
  assign_generated_ipv6_cidr_block = true
  cidr_block                       = "172.20.0.0/16"
  enable_dns_hostnames             = true
  enable_dns_support               = true
  tags = {
    "KubernetesCluster"                          = "kops.speedyrogue.com"
    "Name"                                       = "kops.speedyrogue.com"
    "kubernetes.io/cluster/kops.speedyrogue.com" = "owned"
  }
}

resource "aws_vpc_dhcp_options" "kops-speedyrogue-com" {
  domain_name         = "eu-west-2.compute.internal"
  domain_name_servers = ["AmazonProvidedDNS"]
  tags = {
    "KubernetesCluster"                          = "kops.speedyrogue.com"
    "Name"                                       = "kops.speedyrogue.com"
    "kubernetes.io/cluster/kops.speedyrogue.com" = "owned"
  }
}

resource "aws_vpc_dhcp_options_association" "kops-speedyrogue-com" {
  dhcp_options_id = aws_vpc_dhcp_options.kops-speedyrogue-com.id
  vpc_id          = aws_vpc.kops-speedyrogue-com.id
}

terraform {
  required_version = ">= 0.15.0"
  required_providers {
    aws = {
      "configuration_aliases" = [aws.files]
      "source"                = "hashicorp/aws"
      "version"               = ">= 4.0.0"
    }
  }
}

terraform {
  backend "s3" {
    bucket = "kops-nonprod-state-files"
    key    = "kops-terraform-state-files/terraform.tf"
    region = "eu-west-2"
  }
}